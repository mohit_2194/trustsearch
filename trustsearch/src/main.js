import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import StarRating from 'vue-star-rating'
import VueCarousel from 'vue-carousel';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faTwitter, faFacebook, faGoogle, faLinkedin } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faBars)
library.add(faBars, faTwitter, faFacebook, faGoogle, faLinkedin)

Vue.config.productionTip = false
Vue.component('star-rating', StarRating);
Vue.use(VueCarousel);
Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
